function showResponseText(json) {
//	document.getElementById("message_body").innerHTML = JSON.stringify(json,
//			null, "\t").replace(/\n/g, "<br>");
//	document.getElementById("message").style.visibility = "visible";
	console.log(JSON.stringify(json, null, "\t"));

}

function onError(json) {
	alert(json.error);
	showResponseText(json)
}

function onSuccess(json) {
	console.log(JSON.stringify(json, null, "\t"));
//	showResponseText(json)
}

function isExpired(dateString) {
	var date = new Date(dateString);
	var jetzt = new Date();
	if(date.getTime() <= jetzt.getTime()) {
		return true;
	}
	return false;
}

function getRequest(url, successCallback, errorCallback) {
	if (errorCallback == null)
		errorCallback = onError;

	if (successCallback == null)
		successCallback = onSuccess;
	try {
		var http = new XMLHttpRequest();
		http.open("GET", url, true);
		http.onreadystatechange = function() {
			if (http.readyState == 4 && http.status == 200) {
				var txt = http.responseText;
				var json = JSON.parse(txt);
				if (json.error && json.error != "") {
					errorCallback(json);
				} else {
					successCallback(json);
				}
			} else if (http.readyState == 4 && http.status >= 400) {
				var err = http.responseText;
				errorCallback({
					"error" : err
				});
			}
		}
		http.send();
	} catch (e) {
		errorCallback({
			"error" : e
		});
	}
}

function postRequest(json, url, successCallback, errorCallback) {	
	if (errorCallback == null)
		errorCallback = onError;

	if (successCallback == null)
		successCallback = onSuccess;

	try {
		var http = new XMLHttpRequest();
		http.open("POST", url, true);
		http.setRequestHeader("Content-Type", "application/json");
		http.onreadystatechange = function() {
			if (http.readyState == 4 && http.status == 200) {
				var txt = http.responseText;
				var json = JSON.parse(txt);
				if (json.error && json.error != "") {
					errorCallback(json);
				} else {
					successCallback(json);
				}
			} else if (http.readyState == 4 && http.status >= 400) {
				var err = http.responseText;
				errorCallback({
					"error" : err
				});
			}
		}
		http.send(JSON.stringify(json));
	} catch (e) {
		errorCallback({
			"error" : e
		});
	}
}