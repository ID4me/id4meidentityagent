var accounts = [];

function init() {
	getRequest("/session", onSessionSuccess, onSessionError);
}

function onSessionSuccess(json) {
	for (i in json.authorities) {
		if ("ADMIN" == json.authorities[i].authority) {
			console.log("IDENTITY IS ADMIN");
			getAgentList();
			return;
		}
	}
	alert("sorry, you're not listed as admin!");
	document.location = "/";
}

function onSessionError(json) {
	console.log("Error:" + JSON.stringify(json, null, "\t"));
	alert(json.error);
	document.location = "/";
}

function registerNewAgent() {
	var f = document.forms["register_agent"];
	var json = {};
	json["name"] = f.name.value;
	json["fullName"] = f.fullName.value;
	json["email"] = f.email.value;
	json["issuerURL"] = f.issuerURL.value;
	json["aimURL"] = f.aimURL.value;
	if (f.name.value == "" || f.fullName.value == "" || f.email.value == ""
			|| f.issuerURL.value == "" || f.aimURL.value == "") {
		alert("You must fill in all form fields!");
		return;
	}
	if (isAgentAlreadyKnown(f.name.value)) {
		alert("An agent with this name is already registered!");
		return;
	}

	var url = "/agent/register";
	postRequest(json, url, function(json) {
		document.location.reload();
	});
}

function getAgentList() {
	var url = "/agent/list";
	getRequest(url, onAgentListSuccess);
}

function onAgentListSuccess(json) {
	accounts = json;
	console.log(JSON.stringify(accounts, null, "\t"));
	fillAgentDropdown();
}

function fetchAgentData() {
	var agentid = document.forms["edit_agent"]["id"].value;
	var url = "/agent/fetch/" + agentid;
	getRequest(url);
}

function updateAgentData() {
	var f = document.forms["edit_agent"];
	var json = {};
	json["id"] = f.id.value;
	json["name"] = f.agents[f.agents.selectedIndex].name;
	json["fullName"] = f.fullName.value;
	json["email"] = f.email.value;
	json["issuerURL"] = f.issuerURL.value;
	json["aimURL"] = f.aimURL.value;
	var url = "/agent/update";
	postRequest(json, url);
}

function isAgentAlreadyKnown(name) {
	var select = document.forms["edit_agent"]["agents"].options;
	for (i in accounts) {
		if (name == accounts[i].name) {
			for (var i = 0; i < select.length; i++) {
				if (name == select[i].name) {
					document.forms["edit_agent"]["agents"].selectedIndex = i;
					console.log("FOUND: " + name);
				}
			}
			return true;
		}
	}
	return false;
}

function onAgentDropdownChange(select) {
	console.log("select: " + select);
	var json = JSON.parse(select);
	var f = document.forms["edit_agent"];
	f.id.value = json.id;
	f.status.value = json.status;
	f.issuerURL.value = json.issuerURL;
	f.fullName.value = json.fullName;
	f.email.value = json.email;
	f.aimURL.value = json.aimURL;
}

function fillAgentDropdown() {
	var f = document.forms["edit_agent"];
	var elem = f.agents;
	for (i in accounts) {
		var option = document.createElement("option");
		option.value = JSON.stringify(accounts[i]);
		option.name = accounts[i].name;
		option.innerHTML = accounts[i].name;
		elem.appendChild(option);
	}
	onAgentDropdownChange(elem[0].value);
}