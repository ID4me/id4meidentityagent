function init() {
	fetchUserinfoData();
}

function fetchUserinfoData() {
	var url = "/userinfos/fetch";
	getRequest(url, onFetchUserinfoData, onFetchUserinfoDataError);
}

function onFetchUserinfoDataError(json) {
	alert(json.error);
	document.location = "/";
}

function onFetchUserinfoData(json) {
	showResponseText(json);
	var f = document.forms["edit_userinfo"];
	f.identifier.value = json.identifier;
	f.website.value = json.website;
	f.zoneinfo.value = json.zoneinfo;
	f.email_verified.value = json.email_verified;
	f.birthdate.value = json.birthdate;
	f.address.value = json.address;
	f.gender.value = json.gender;
	f.profile.value = json.profile;
	f.phone_number_verified.value = json.phone_number_verified;
	f.preferred_username.value = json.preferred_username;
	f.given_name.value = json.given_name;
	f.middle_name.value = json.middle_name;
	f.locale.value = json.locale;
	f.picture.value = json.picture;
	f.name.value = json.name;
	f.nickname.value = json.nickname;
	f.phone_number.value = json.phone_number;
	f.family_name.value = json.family_name;
	f.email.value = json.email;
}

function saveUserinfoData() {
	var f = document.forms["edit_userinfo"];
	var json = {
			"identifier":f.identifier.value,
			"website":f.website.value,
			"zoneinfo":f.zoneinfo.value,
			"email_verified":f.email_verified.value,
			"birthdate":f.birthdate.value,
			"address":f.address.value,
			"gender":f.gender.value,
			"profile":f.profile.value,
			"phone_number_verified":f.phone_number_verified.value,
			"preferred_username":f.preferred_username.value,
			"given_name":f.given_name.value,
			"middle_name":f.middle_name.value,
			"locale":f.locale.value,
			"picture":f.picture.value,
			"name":f.name.value,
			"nickname":f.nickname.value,
			"phone_number":f.phone_number.value,
			"family_name":f.family_name.value,
			"email":f.email.value		
	}
	var url = "/userinfos/save";
	postRequest(json, url, onSaveUserinfoData);
}

function onSaveUserinfoData(json) {
	showResponseText(json);
}