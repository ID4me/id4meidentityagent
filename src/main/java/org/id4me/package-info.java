/**
 * org.id4me contains an example Spring Boot web application to create and
 * manage an ID4me Agent/CLP and its related identities.
 * 
 * The the application.properties must contain any parameters to configure the
 * relying-party-api and the authorization filters behavior. <br>
 * Example application.properties:
 * 
 * <pre>
#Comma separated identifiers of administrators. 
#Is needed to create agent registrations.
id4me.admins=admin.example.org

#A local file path with read/write access for ID4meDataImpl.class
id4me.data.path=/opt/id4me 
#A private key file in PEM format. Used by ID4meDataImpl.class
id4me.private.key=/opt/id4me/id4me.priv.pem
#A public key file in PEM format. Used by ID4meDataImpl.class
id4me.public.key=/opt/id4me/id4me.pub.pem
#Path to jwks.json. The file will be created if not found.
jwks.json=/opt/id4me/jwks.json

#The iss for ./well-known/openid-configurationn attributes issuer,jwks_uri, userinfo_endpoint.
id4me.iss=https://example.org

#Comma separated list of urls with anonymous access. DO NOT CHANGE WITHOUT SPECIFIC REASON!
id4me.excluded.urls=/userinfo,/.well-known/openid-configuration,/openid-configuration,/jwks.json,/,/favicon.ico,/js/*.*,/img/*.*,/css/*.*,/identity.html,/agent/list,/identities/create,/authz/*.*

#id4me properties values for the relying party filter configuration
registration.data.path=/opt/id4me/registrations
logo.uri=
redirect.uri=https\://example.org/logon
dnsssec_root_key=. IN DS 20326 8 2 E06D44B80B8F1D39A95C0B0D7C65D08458E880409BBC683457104237C7F8EC8D
client.name=Claims-Provider-Demo
client.max_fetch_size=50000
dns.resolver=8.8.8.8
dnssec_required=false
scopes_fallback=true
 * </pre>
 * 
 */
package org.id4me;
