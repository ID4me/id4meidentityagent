/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me;

import javax.servlet.Filter;

import org.id4me.impl.ID4meDataImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Spring Boot application using the ID4me authentication filter to log on at an
 * ID4me authorization.
 * 
 * @author peter.hoebel@open-xchange.com
 *
 */
@SpringBootApplication
public class ID4meIdentityAgentApplication extends WebSecurityConfigurerAdapter {
	@Autowired
	private Environment env;

	public static void main(String[] args) {
		SpringApplication.run(ID4meIdentityAgentApplication.class, args);
	}

// this bean is no longer used and only for documentation purpose not deleted
// @Bean
//	public FilterRegistrationBean<ID4meRelyingPartyFilter> authenticationFilter() {
//		FilterRegistrationBean<ID4meRelyingPartyFilter> registrationBean = new FilterRegistrationBean<>();
//		registrationBean.setFilter(new ID4meRelyingPartyFilter(env));
//		registrationBean.addUrlPatterns("/**");
//	return registrationBean;
//	}

	/**
	 * Configure the authentication filter behavior and create the jwks.json file, if not exists.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ID4meDataImpl data = new ID4meDataImpl(env);
		data.createJwkIfNotExists();

		http.addFilterBefore(ssoFilter(), UsernamePasswordAuthenticationFilter.class);
		http.csrf().disable().cors().and().exceptionHandling()
				.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and().logout().disable();
	}

	private Filter ssoFilter() {
		return new ID4meRelyingPartyFilter(env);
	}

}
