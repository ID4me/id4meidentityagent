/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.id4me.utils.ID4meUtils;
import org.id4me.utils.ID4meTokenHandler;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring Boot controller with the endpoints used to implement a miminal ID4me
 * claims provider.
 * 
 * @author peter.hoebel
 *
 */
@RestController
public class ID4meUserinfoController {
	private static final Logger log = Logger.getGlobal();

	@Autowired
	private Environment env;

	/**
	 * .well-known/openid-configuration endpoint, called by the relying party. The
	 * The minimal openid-configuration file consists of the fields:
	 * claims_supported, id_token_signing_alg_values_supported, issuer, jwks_uri,
	 * response_types_supported and userinfo_endpoint. The file is located at the
	 * ressources path of the project.
	 * 
	 * @param req
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/.well-known/openid-configuration", produces = "application/json")
	public String openidConfiguration(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		log.info("/.well-known/openid-configuration");
		ClassPathResource cpr = new ClassPathResource("static/openid-configuration");
		byte[] buff = FileCopyUtils.copyToByteArray(cpr.getInputStream());
		String json_str = new String(buff, "utf8");
		JSONObject json = new JSONObject(json_str);
		String iss = env.getProperty("id4me.iss");
		json.put("issuer", iss);
		json.put("jwks_uri", iss + "/jwks.json");
		json.put("userinfo_endpoint", iss + "/userinfo");
		return json.toString(2);
	}

	@RequestMapping(value="/jwks.json", produces = "application/json")
	public String jwksJson() throws Exception {
		ID4meUtils utils = new ID4meUtils(env);		
		return utils.getJwksJson();
	}
	
	/**
	 * Userinfo endpoint, called by the relying party. The userinfo is a simple json
	 * object
	 * 
	 * @param HttpServletRequest with a bearer token in authorization header
	 * 
	 * @return A signed jwt containing the userinfo JSON object
	 * @throws Exception
	 */
	@RequestMapping(value = "/userinfo", produces = "application/json")
	public String userinfo(HttpServletRequest req) throws Exception {
		log.info("/userinfo");

		String auth = req.getHeader("Authorization");
		log.info("Authorization: " + auth);

		// if the request has no bearer token we send an error back
		if (auth != null && auth.toLowerCase().startsWith("bearer")) {
			String signedToken = "";
			String token = auth.split("\\s")[1];
			ID4meTokenHandler parser = new ID4meTokenHandler(env);
			parser.parseToken(token);
			parser.valiateToken();

			JSONObject reqPayload = parser.getPayload();
			log.info("PARSED HEADER: " + parser.getHeader().toString());
			log.info("PARSED PAYLOAD: " + parser.getPayload().toString());

			String identifier = reqPayload.getString("id4me.identifier");
			ID4meUtils utils = new ID4meUtils(env);

			// Get the userinfo json object
			String payload = utils.getUserinfos(identifier);
			// Create a signed token with this userinfo to send back to the RP
			JSONObject userinfo = new JSONObject(payload);
			// Time values in seconds from 1970-01-01T0:0:0Z
			int exp = (int) ((System.currentTimeMillis() + (1000 * 60 * 60)) * 0.001);
			int iat = (int) ((System.currentTimeMillis() + (1000 * 60 * 60)) * 0.001);
			userinfo.put("exp", exp);
			userinfo.put("iat", iat);

			userinfo.put("iss", env.getProperty("id4me.iss"));
			// Get a static jwks.json because the kid would change if we compute a new jwks
			// from the key			
			JSONObject jwks = new JSONObject(utils.getJwksJson());
			// we just take the first key from the list
			JSONObject jwk = jwks.getJSONArray("keys").getJSONObject(0);
			signedToken = parser.createSignedToken(jwk.getString("kid"), userinfo.toString());
			return signedToken;
		} else {
			log.info("No bearer token found: " + auth);
			return "{\"error\": \"No bearer token found in [" + auth + "]\"}";
		}
	}

}
