/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/
package org.id4me;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.id4me.config.Id4meClaimsParameters;
import org.id4me.config.Id4meClaimsParameters.Entry;
import org.id4me.config.Id4meProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Spring Boot authentication filter to log on with an ID4me Identity. The
 * filter injects a Principal object into the SecurityContextHolder and a Cookie
 * into the http response after a user has successfully authenticated at an
 * ID4me authority. <br>
 * It uses @see <a href=
 * "https://search.maven.org/artifact/org.id4me/relying-party-api/2.15/jar">relying-party-api/2.15/jar</a>
 * for the relying party service.
 * 
 * @author peter.hoebel
 *
 */
@Component
public class ID4meRelyingPartyFilter implements Filter {
	private static final Logger log = Logger.getGlobal();
	private static final String HTML_TOP = "<!DOCTYPE html><html><head></head><body>";
	private static final String HTML_BOTTOM = "</body></html>";
	private String[] EXCLUDE_URLS;
	private String[] ADMINS;
	private Id4meProperties ID4ME_PROPS;
	private Id4meClaimsParameters CLAIMS_PARAMETERS;

	public ID4meRelyingPartyFilter(Environment env) {
		String admins = env.getProperty("id4me.admins");
		ADMINS = admins.split(",");
		String urls = env.getProperty("id4me.excluded.urls");
		EXCLUDE_URLS = urls.split(",");
		ID4ME_PROPS = new Id4meProperties().setRegistrationDataPath(env.getProperty("registration.data.path"))
				.setLogoURI(env.getProperty("logo.uri")).setRedirectURI(env.getProperty("redirect.uri"))
				.setDnssecRootKey(env.getProperty("dnsssec_root_key")).setClientName(env.getProperty("client.name"))
				.setDnsResolver(env.getProperty("dns.resolver"))
				.setDnssecRequired("true".equals(env.getProperty("dnssec_required")) ? true : false)
				.setFallbackToScopes("true".equals(env.getProperty("scopes_fallback")) ? true : false);
		CLAIMS_PARAMETERS = new Id4meClaimsParameters()
				.addEntry((new Entry()).setName("email").setReason("Used to send confirmation email."));
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		return;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		String uri = req.getRequestURI();
		switch (uri) {
		case "/logon.html":
			break;
		case "/login":
			doLogin(req, resp);
			return;
		case "/logon":
			doLogon(req, resp);
			return;
		case "/validate":
			doValidate(req, resp);
			return;
		case "/logout":
			doLogout(req, resp);
			return;
		case "/error":
			break;
		default:
			boolean valid = isLogonValid(req, resp);
			if (valid && SecurityContextHolder.getContext().getAuthentication() != null) {
				log.info("Authenticated: " + SecurityContextHolder.getContext().getAuthentication().isAuthenticated());
				SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
				break;
			} else {
				for (String ex : EXCLUDE_URLS) {
					if (uri.matches(ex)) {
						filterChain.doFilter(request, response);
						return;
					}
				}
			}
			// user is not authenticated and url is not in excluded urls list
			HttpSession session = req.getSession();
			session.setAttribute("redirect_uri", uri);
			resp.sendRedirect("/logon.html");
			return;

		}
		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

	private void doLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.info("/login");
		try {
			String ident = req.getParameter("identity");
			if (ident == null || "".equals(ident.trim()))
				sendHtmlResponse(resp, "Error: no id4me identity provided!");

			HttpSession session = req.getSession();
			Id4meLogon id4me_logon = new Id4meLogon(ID4ME_PROPS, CLAIMS_PARAMETERS);
			log.info("LOGON USER: " + ident);
			Id4meSessionData id4me_session = id4me_logon.createSessionData(ident, true);

			if (id4me_session != null) {
				session.setAttribute("id4me_session", id4me_session);
				session.setAttribute("id4me_logon", id4me_logon);
				String authorizationUri = id4me_logon.authorize(id4me_session);
				resp.sendRedirect(URLDecoder.decode(authorizationUri, "utf-8"));
				return;
			}
			sendHtmlResponse(resp, "Error: id4me session == NULL");
		} catch (Exception ex) {
			log.info("doLogin, error: " + ex.toString());
			resp.setStatus(401);
			sendHtmlResponse(resp, ex.getMessage());
		}
		return;

	}

	private void doLogon(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.info("/logon");
		HttpSession session = req.getSession(false);
		if (session == null) {
			log.info("session expired");
			resp.sendRedirect("/");
			return;
		}

		String error = req.getParameter("error");
		if (error != null) {
			String descr = req.getParameter("error_description");
			log.info("/logon: " + descr);
			req.getSession().invalidate();
			resp.setStatus(401);
			sendHtmlResponse(resp, "Error: " + descr);
			return;
		}
		String code = req.getParameter("code");
		if (code == null || "".equals(code)) {
			resp.setStatus(401);
			sendHtmlResponse(resp, "Error: parameter code missing!");
			return;
		}

		Id4meSessionData id4me_session = (Id4meSessionData) req.getSession(false).getAttribute("id4me_session");
		Id4meLogon logon_handler = (Id4meLogon) req.getSession(false).getAttribute("id4me_logon");

		try {
			logon_handler.authenticate(id4me_session, code);

			log.info("IdentityHandle: " + id4me_session.getIdentityHandle());
			resp.sendRedirect("/validate");
			return;
		} catch (Exception ex) {
			log.info("doLogin, error: " + ex.toString());
			resp.setStatus(401);
			sendHtmlResponse(resp, ex.toString());
		}

	}

	private void doValidate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.info("/validate");
		HttpSession session = req.getSession(false);
		if (session == null) {
			log.info("session expired");
			resp.sendRedirect("/");
			return;
		}

		Id4meSessionData id4me_session = (Id4meSessionData) session.getAttribute("id4me_session");
		if (id4me_session != null) {
			try {
				log.info("session found in request");
				long expires = id4me_session.getTokenExpires();

				if (expires == 0 || expires > System.currentTimeMillis()) {
					if (expires != 0) {
						double e = (expires - System.currentTimeMillis());
						log.info("token for " + id4me_session.getLoginHint() + " expires in: " + Math.round(e * 0.001)
								+ " sec.");
					}

					String id_token = id4me_session.getIdToken();
					if (id_token != null) {
						Id4meLogon id4me_logon = (Id4meLogon) session.getAttribute("id4me_logon");
						// call validates the access_token and throws an Ecxeption if it's failed
						String identity_handle = id4me_logon.identityHandleFromIdToken(id4me_session, id_token);
						log.info("id token for " + identity_handle + " validated.");
						long time = expires - System.currentTimeMillis();
						log.info(
								"Logon expires: " + Math.round(time * 0.001) + " sec. " + new Date(expires).toString());

						String uri = (String) session.getAttribute("redirect_uri");
						session.removeAttribute("redirect_uri");
						setAuthentified(resp, id4me_session);

						if (uri != null)
							resp.sendRedirect(uri);
						else
							resp.sendRedirect("/");
						return;
					}
					log.info("doValidate, error: No token in id4me logon found!");
					resp.setStatus(401);
					sendHtmlResponse(resp, "No token in id4me logon found!");
					return;
				} else {
					log.info("doValidate, error: Token expired!");
					resp.setStatus(401);
					sendHtmlResponse(resp, "Token expired!");
					return;
				}
			} catch (Exception ex) {
				log.info("doValidate, error: " + ex.toString());
				resp.setStatus(401);
				sendHtmlResponse(resp, ex.toString());
				return;
			}
		}

		log.info("doValidate, error: No id4me in session found!");
		resp.setStatus(401);
		sendHtmlResponse(resp, "No id4me in session found!");
	}

	private boolean isLogonValid(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		if (SecurityContextHolder.getContext().getAuthentication() != null)
			SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);

		HttpSession session = req.getSession(false);
		if (session == null) {
			log.info("session expired");
			// remove cookie
			setLogonCookie(resp, null);
			return false;
		}

		Id4meSessionData id4me_session = (Id4meSessionData) session.getAttribute("id4me_session");
		if (id4me_session != null) {
			try {
				log.info("session found in request");
				long expires = id4me_session.getTokenExpires();
				if (expires == 0 || expires > System.currentTimeMillis()) {
					if (expires != 0) {
						double e = (expires - System.currentTimeMillis());
						log.info("token for " + id4me_session.getLoginHint() + " expires in: " + Math.round(e * 0.001)
								+ " sec.");
					}

					String id_token = id4me_session.getIdToken();
					if (id_token != null) {
						Id4meLogon id4me_logon = (Id4meLogon) session.getAttribute("id4me_logon");
						// call validates the access_token and throws an Ecxeption if it's failed
						String identity_handle = id4me_logon.identityHandleFromIdToken(id4me_session, id_token);
						log.info("id token for " + identity_handle + " validated.");
						long time = expires - System.currentTimeMillis();
						log.info(
								"Logon expires: " + Math.round(time * 0.001) + " sec. " + new Date(expires).toString());

						setAuthentified(resp, id4me_session);
						return true;
					}
				}
			} catch (Exception ex) {
			}
		}
		// remove cookie
		setLogonCookie(resp, null);
		return false;
	}

	private void doLogout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.info("/logout");
		if (SecurityContextHolder.getContext().getAuthentication() != null)
			SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);

		// remove cookie from session
		setLogonCookie(resp, null);

		HttpSession session = req.getSession(false);
		if (session == null) {
			resp.sendRedirect("/");
			return;
		}

		session.removeAttribute("redirect_uri");
		session.removeAttribute("id4me_session");
		session.removeAttribute("id4me_logon");
		session.invalidate();
		resp.sendRedirect("/");
		return;
	}

	private void setLogonCookie(HttpServletResponse resp, Id4meSessionData id4me_session) {
		if (id4me_session == null) {
			resp.setHeader("set-cookie", "id4me.identifier=; Path=/;");
			resp.setHeader("set-cookie2", "id4me.identifier=; Path=/;");
			log.info("setLogonCookie: set-cookie\", \"id4me.identifier=; Path=/;");
//			Cookie cookie = new Cookie("id4me.identifier", "");
//			cookie.setMaxAge(0);
//			cookie.setPath("/");
//			resp.addCookie(cookie);
		} else {
			resp.setHeader("set-cookie", "id4me.identifier=" + id4me_session.getId4me() + "; Path=/;");
			resp.setHeader("set-cookie2", "id4me.identifier=" + id4me_session.getId4me() + "; Path=/;");
			log.info(
					"setLogonCookie: \"set-cookie\", \"id4me.identifier=\" + id4me_session.getId4me() + \"; Path=/;\"");
//			Cookie cookie = new Cookie("id4me.identifier", id4me_session.getId4me());
//			cookie.setMaxAge(-1);
//			cookie.setPath("/");
//			resp.addCookie(cookie);
		}
	}

	private void sendHtmlResponse(HttpServletResponse resp, String response) throws IOException {
		if (response == null)
			return;
		PrintWriter writer = resp.getWriter();
		resp.setContentType("text/html");
		writer.print(HTML_TOP + response + HTML_BOTTOM);
		writer.flush();
		writer.close();
	}

	private void setAuthentified(HttpServletResponse resp, Id4meSessionData id4me_session) {
		UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) SecurityContextHolder
				.getContext().getAuthentication();
		JSONObject idTokenJson = id4me_session.getIdTokenUserinfo();

		List<GrantedAuthority> grantedAuths;
		// example how to set roles
		grantedAuths = Collections.emptyList();
		for (String admin : ADMINS) {
			String identity = id4me_session.getId4me();
			if (admin.trim().equals(identity)) {
				grantedAuths = AuthorityUtils.commaSeparatedStringToAuthorityList("ADMIN");
			}
		}

		HashMap<String, String> credentials = new HashMap<String, String>();
		credentials.put("id_token", id4me_session.getIdToken());
		credentials.put("access_token", id4me_session.getAccessToken());
		credentials.put("identity_handle", id4me_session.getIdentityHandle());
		long exp = id4me_session.getTokenExpires();
		Date date = new Date(exp);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
		if (exp != 0)
			credentials.put("token_expires", format.format(date));

		authenticationToken = new UsernamePasswordAuthenticationToken(id4me_session.getId4me(), credentials,
				grantedAuths);

		HashMap<String, String> details = new HashMap<String, String>();
		details.put("iss", idTokenJson.getString("iss"));
		details.put("sub", idTokenJson.getString("sub"));
		Object aud = idTokenJson.get("aud");
		if (aud instanceof JSONArray) {
			ArrayList<String> ad = new ArrayList<String>();
			for (Object a : idTokenJson.getJSONArray("aud")) {
				ad.add(a.toString());
			}
			details.put("aud", ad.toString());
		} else {
			details.put("aud", idTokenJson.getString("aud"));
		}
		setLogonCookie(resp, id4me_session);
		authenticationToken.setDetails(details);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
	}

}