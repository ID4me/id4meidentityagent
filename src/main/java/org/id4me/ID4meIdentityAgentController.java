/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

/**
 * org.id4me.demo contains the spring boot application and controller classes, used in the <b>ID4meAimDemoProject</b>.
 */
package org.id4me;

import java.security.Principal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring Boot web service controller to show how to access the Principal object
 * from the user session.
 * 
 * @author peter.hoebel@open-xchange.com
 *
 */
@RestController
public class ID4meIdentityAgentController {
	@RequestMapping(value = "/session", produces = "application/json")
	public Principal principal(Principal principal) throws Exception {
		return principal;
	}

}
