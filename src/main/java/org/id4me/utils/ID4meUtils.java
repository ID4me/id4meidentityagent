package org.id4me.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.net.ssl.HttpsURLConnection;

import org.id4me.impl.ID4meDataImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.BoundedInputStream;

public class ID4meUtils {
	private static int MAX_FETCH_SIZE = 50000;
	private ID4meData data;
	private static final Logger log = LoggerFactory.getLogger(ID4meUtils.class);

	/**
	 * Constructor, creates all used directories and sets some local variables.
	 * 
	 * @param env
	 */
	public ID4meUtils(Environment env) throws Exception {
		this.data = new ID4meDataImpl(env);
	}

	/**
	 * Returns a list of identity authorization which are not expired or already
	 * solved.
	 * 
	 * @return JSONArray list of identity authorizations
	 * @throws Exception
	 */
	public JSONArray listIdentityRegistrations() throws Exception {
		return data.listAgentRegistrations();
	}

	public String getIdentityAuthorization(String identifier) throws Exception {
		return data.getIdentityAuthorization(identifier);
	}

	public void saveIdentityAuthorization(JSONObject registration) throws Exception {
		data.saveIdentityAuthorization(registration);
	}

	public void saveIdentityDetails(JSONObject registration) throws Exception {
		data.saveIdentityDetails(registration);
	}

	public JSONArray listAgentRegistrations() throws Exception {
		return data.listAgentRegistrations();
	}

	public void saveAgentRegistration(JSONObject registration) throws Exception {
		data.saveAgentRegistration(registration);
	}

	public String getAgentRegistration(String agentId) throws Exception {
		return data.getAgentRegistration(agentId);
	}

	/**
	 * Returns the userinfo object for the identity. If no data is found, a default
	 * userinfo object containing all supported claims is returned.
	 * 
	 * @return String representation of the json object
	 */
	public String getUserinfos(String name) {
		return data.getUserinfos(name);
	}

	public void saveUserinfos(String identifier, JSONObject userinfo) throws Exception {
		data.saveUserinfos(identifier, userinfo);
	}

	public PrivateKey getPrivateKey() throws Exception {
		return data.getPrivateKey();
	}

	public PublicKey getPublicKey() throws Exception {
		return data.getPublicKey();
	}

	public String getJwksJson() throws Exception {
		return data.getJwksJson();
	}
	/**
	 * Computes a sha256 of the provided string. This call is used to compute the
	 * _acme-challenge record for the token of an identity authorization.
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public String sha256(String token) throws Exception {
		String json = data.getJwksJson();
		JSONObject keys = new JSONObject(json);
		JWK jwk = JWK.parse(keys.getJSONArray("keys").getJSONObject(0).toString());
		Base64URL thumbprint = jwk.computeThumbprint("SHA-256");
		MessageDigest hasher = MessageDigest.getInstance("SHA-256");
		hasher.update((token + "." + thumbprint).getBytes());
		byte[] fingerprint = hasher.digest();
		String hash = Base64.getEncoder().withoutPadding().encodeToString(fingerprint);
		hash = Base64.getUrlEncoder().withoutPadding().encodeToString(fingerprint);
		return hash;
	}

	/**
	 * Test, if the json object has a field expired and if so, if it is expired.
	 * This method is used by {@link #listIdentityRegistrations()} to check the
	 * identity authorization data
	 * 
	 * @param json
	 * @return true if expired is available and the value before now
	 * @throws Exception
	 */
	public boolean isExpired(JSONObject json) throws Exception {
		return isExpired(json, "expires");
	}

	/**
	 * Test, if the json object has a field url_expire and if so, if it is expired.
	 * This method is used to check if the magic url in identity authorization data
	 * is expired.
	 * 
	 * @param json
	 * @return true if url_expire is available and the value before now
	 * @throws Exception
	 */
	public boolean isUrlExpired(JSONObject json) throws Exception {
		return isExpired(json, "url_expire");
	}

	/**
	 * Retreives the content from a https url.
	 * 
	 * @param httpsUrl
	 * @return the response body from the https request
	 * @throws IOException
	 */
	public String fetchUrl(String httpsUrl) throws IOException {
		URL url = new URL(httpsUrl);
		log.info("fetchUrl(), httpsUrl: " + httpsUrl);

		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		con.setInstanceFollowRedirects(true);
		log.debug("fetchUrl: " + con.getResponseCode() + ", " + con.getResponseMessage());

		InputStream bounded = new BoundedInputStream(con.getInputStream(), MAX_FETCH_SIZE);
		StringBuilder response = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(bounded))) {
			String input;
			while ((input = br.readLine()) != null) {
				response.append(input);
			}
		}
		log.debug("Fetched from URL:          {}", httpsUrl);
		return response.toString();
	}

	private boolean isExpired(JSONObject json, String attribute) throws Exception {
		if (json.has(attribute)) {
			Object exp = json.get(attribute);
			long time;
			if (exp instanceof String) {
				String expires = (String) exp;
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date = format.parse(expires);
				time = date.getTime();
				int offset = GregorianCalendar.getInstance().getTimeZone().getOffset(date.getTime());
				time += offset;
			} else {
				exp = json.getLong(attribute);
				time = (Long) exp;
				time = time * 1000; // linux time is seconds since 01.01.1970, java milliseconds
			}
			if (time < System.currentTimeMillis())
				return true;
			else
				return false;
		}
		return false;
	}

}
