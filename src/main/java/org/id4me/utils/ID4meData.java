package org.id4me.utils;

import java.security.PrivateKey;
import java.security.PublicKey;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This interface privides all methods to manage the agent registration data,
 * the identity authorization data and the /userinfo data,
 * 
 * @author phoebel
 *
 */
public interface ID4meData {

	/**
	 * Return the identity authorization data for the given identifier.
	 * 
	 * @param identifier
	 * @return
	 * @throws Exception
	 */
	String getIdentityAuthorization(String identifier) throws Exception;

	/**
	 * Save the identity authorization data.
	 * 
	 * @param registration
	 * @throws Exception
	 */
	void saveIdentityAuthorization(JSONObject registration) throws Exception;

	/**
	 * Save the identity registration details.
	 * 
	 * @param registration
	 * @throws Exception
	 */
	void saveIdentityDetails(JSONObject registration) throws Exception;

	/**
	 * Return a list of agent registrations.
	 * 
	 * @return
	 * @throws Exception
	 */
	JSONArray listAgentRegistrations() throws Exception;

	/**
	 * Save agent registration data.
	 * 
	 * @param registration
	 * @throws Exception
	 */
	void saveAgentRegistration(JSONObject registration) throws Exception;

	/**
	 * Return the agent registration data for an agent id.
	 * 
	 * @param agentId
	 * @return
	 * @throws Exception
	 */
	String getAgentRegistration(String agentId) throws Exception;

	/**
	 * Return the userinfo data for a given identity, or if not found the default
	 * userinfo data.
	 * 
	 * @param name
	 * @return
	 */
	String getUserinfos(String name);

	/**
	 * Save the userinfo data for an ID4me identifier.
	 * 
	 * @param identifier
	 * @param userinfo
	 * @throws Exception
	 */
	void saveUserinfos(String identifier, JSONObject userinfo) throws Exception;

	/**
	 * Return the private key which is used by this servers instance.
	 * 
	 * @return
	 * @throws Exception
	 */
	PrivateKey getPrivateKey() throws Exception;

	/**
	 * Return the public key which is used by this servers instance.
	 * 
	 * @return
	 * @throws Exception
	 */
	PublicKey getPublicKey() throws Exception;

	/**
	 * Return the jwks.json which is used by this servers instance.
	 * 
	 * @return
	 * @throws Exception
	 */
	String getJwksJson() throws Exception;
	
	/**
	 * Create jwks.json file, if not exists at the path, configured in environment property jwks.json.
	 * 
	 * @return
	 * @throws Exception
	 */
	void createJwkIfNotExists() throws Exception;

}