package org.id4me.utils;

import java.security.interfaces.RSAPublicKey;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.lang.JoseException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;

/**
 * Contains any methods, used by the application, to create and validate java
 * web token.
 * 
 * @author peter.hoebel
 *
 */
public class ID4meTokenHandler {
	private static final Logger log = LoggerFactory.getLogger(ID4meTokenHandler.class);
	private boolean isValidated = false;
	private SignedJWT signedToken = null;
	private JSONObject headerJson = null;
	private JSONObject payloadJson = null;
	private String dataPath;
	private ID4meUtils utils;

	public ID4meTokenHandler(Environment env) throws Exception {
		this.dataPath = env.getProperty("id4me.data.path");
		this.dataPath = this.dataPath.endsWith("/") ? this.dataPath : this.dataPath.trim() + "/";
		this.utils = new ID4meUtils(env);
	}

	public JSONObject getHeader() {
		if (isValidated)
			return headerJson;
		return null;
	}

	public JSONObject getPayload() {
		if (isValidated)
			return payloadJson;
		return null;
	}

	/**
	 * Computes a JWS Compact Serialization from a json web signature with the
	 * provided payload.
	 * 
	 * @param kid     used in the jws kid header field
	 * @param payload for the jws
	 * @return JWS Compact Serialization String
	 * @throws Exception
	 */
	public String createSignedToken(String kid, String payload) throws Exception {
		JsonWebSignature jws = new JsonWebSignature();
		jws.setKey(utils.getPrivateKey());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
		jws.setHeader("kid", kid);
		jws.setPayload(payload);

		String jwsCompactSerialization = null;
		try {
			jwsCompactSerialization = jws.getCompactSerialization();
		} catch (JoseException e) {
			e.printStackTrace();
		}

		return jwsCompactSerialization;
	}

	/**
	 * Validates the signed token from a previously called  {@link #parseToken(String)} call against the
	 * jwks data from the issuer. Therefore the current jwks is retreived from the
	 * issuer.
	 * 
	 * @throws Exception
	 */
	public void valiateToken() throws Exception {
		log.info("valiateToken(), PAYLOAD: " + payloadJson.toString());
		String iss = payloadJson.getString("iss");
		log.info("valiateToken(), ISS: " + iss);
		JSONObject config = fetchWellKnownData(iss);
		String jwks_uri = config.getString("jwks_uri");
		JSONObject jwtsData = fetchJwtsData(jwks_uri);
		log.debug("SIGNED TOKEN: " + signedToken.getParsedString());
		validateSignedToken(jwtsData, signedToken);
		isValidated = true;
	}

	/**
	 * Parses the given token and stores the results in class variables for further
	 * use.
	 * 
	 * @param token
	 * @throws Exception
	 */
	public void parseToken(String token) throws Exception {
		log.info("parseToken(), TOKEN: " + token);
		signedToken = (SignedJWT) JWTParser.parse(token);
		headerJson = new JSONObject(signedToken.getHeader().toString());
		payloadJson = new JSONObject(signedToken.getPayload().toString());
		log.debug("HEADER: " + headerJson.toString(2));
		log.debug("PAYLOAD: " + payloadJson.toString(2));
	}

	private void validateSignedToken(JSONObject jwtsData, SignedJWT signedToken) throws Exception {
		log.debug("Validate signed token:          {}", signedToken.toString());

		String headerKid = null;
		if (headerJson.has("kid")) {
			headerKid = headerJson.getString("kid");
		}

		if (!headerJson.has("alg")) {
			throw new Exception("Field alg missing in token payload!");
		}

		String headerAlg = headerJson.getString("alg");

		if (!headerAlg.equalsIgnoreCase("RS256")) {
			throw new Exception("JWTS signature algorithm mismatch, expected RS256, found " + headerAlg);
		}

		validateTokenSignature(jwtsData, signedToken, headerKid, headerAlg);
	}

	private void validateTokenSignature(JSONObject jwtsData, SignedJWT signedToken, String headerKid, String headerAlg)
			throws Exception {
		JSONArray keys = jwtsData.getJSONArray("keys");
		if (keys == null || keys.length() <= 0) {
			throw new IllegalArgumentException("Error on validating the token, keys == NULL");
		}

		for (int i = 0; i < keys.length(); i++) {
			JSONObject key = keys.getJSONObject(i);
			String kid = key.getString("kid");
			log.debug("Validating token signature: kid: {}", kid);
			if (headerKid == null || kid.equals(headerKid)) {
				switch (headerAlg.toUpperCase()) {
				case "RS256":
					RSAKey rsa = RSAKey.parse(key.toString());
					RSAPublicKey pubKey = (RSAPublicKey) rsa.toPublicKey();
					JWSVerifier verifier = new RSASSAVerifier(pubKey);
					if (signedToken.verify(verifier)) {
						log.debug("Validating token signature: token RS256 signature valid");
						return;
					} else {
						throw new Exception("Error on validating the token signature, kid=RS256, alg=RSA");
					}
				default:
					throw new IllegalArgumentException("Unhandled value for header_alg: " + headerAlg);
				}
			}
		}

		throw new Exception("No valid public key for token validation found!");
	}

	private JSONObject fetchJwtsData(String jwks_uri) throws Exception {
		String jwts = utils.fetchUrl(jwks_uri);
		return new JSONObject(jwts);
	}

	private JSONObject fetchWellKnownData(String iss) throws Exception {
		String config = utils.fetchUrl(iss + "/.well-known/openid-configuration");
		log.info("fetchWellKnownData(), config: " + config);
		return new JSONObject(config);
	}
}
