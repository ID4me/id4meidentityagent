/**
 * org.id4me.utils contains the helper classes for the Spring Boot web application.
 * 
 */
package org.id4me.utils;
