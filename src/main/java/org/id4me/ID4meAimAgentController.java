/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

/**
 * org.id4me.demo contains the spring boot application and controller classes, used in the <b>ID4meAimDemoProject</b>.
 */
package org.id4me;

import java.util.logging.Logger;

import org.id4me.agent.RegistrationHandler;
import org.id4me.utils.ID4meUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring Boot controller with the endpoints related to the agent management calls
 * of @see <a href="https://id.denic.de/aim/docs/api/#tag/agents">Agent Identity
 * Management</a>
 * 
 * @author peter.hoebel
 *
 */
@RestController
public class ID4meAimAgentController {
	private static final Logger log = Logger.getGlobal();
	@Autowired
	private Environment env;

	/**
	 * Create a new agent account registration at the <b>ID4me Authority</b>
	 * 
	 * @param payload The registration data as JSON String. <br>
	 *                Example payload:
	 * 
	 *                <pre>
	 * { 
	 *   "name": "The agent name", 
	 *   "fullName": "The agents full name", 
	 *   "email": "The email address", 
	 *   "issuerURL": "The issuer url for this agent." 
	 * }
	 *                </pre>
	 * 
	 * @return A JSON String of the response text of the api call, or if an error
	 *         occured the payload JSON with the error description in an additional
	 *         attribute "error".
	 * @throws Exception
	 */
	@RequestMapping(value = "/agent/register", consumes = "application/json", produces = "application/json")
	public String registerAgent(@RequestBody String payload) throws Exception {
		log.info("/agent/registert");
		JSONObject json = new JSONObject(payload);

		String aimUrl = json.getString("aimURL");
		ID4meUtils utils = new ID4meUtils(env);
		RegistrationHandler registration = new RegistrationHandler(aimUrl, utils.getPublicKey(), utils.getPrivateKey());

		JSONObject body = new JSONObject();
		body.put("name", json.getString("name"));
		body.put("fullName", json.getString("fullName"));
		body.put("email", json.getString("email"));
		body.put("issuerURL", json.getString("issuerURL"));
		log.info(body.toString(2));

		String response = registration.registerAgent(body.toString());
		log.info(response);
		int statusCode = registration.getStatusCode();
		if (statusCode != 201) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
			json.put("aimURL", aimUrl);
			utils.saveAgentRegistration(json);
		}
		return json.toString();
	}

	@RequestMapping(value = "/agent/update", consumes = "application/json", produces = "application/json")
	public String updateAgent(@RequestBody String payload) throws Exception {
		log.info("/agent/update");
		JSONObject body = new JSONObject(payload);
		log.info(body.toString(2));

		String agentId = body.getString("id");
		String aimUrl = body.getString("aimURL");
		ID4meUtils utils = new ID4meUtils(env);
		RegistrationHandler registration = new RegistrationHandler(aimUrl, utils.getPublicKey(), utils.getPrivateKey());

		JSONObject json = new JSONObject();
		json.put("name", body.getString("name"));
		json.put("fullName", body.getString("fullName"));
		json.put("email", body.getString("email"));
		json.put("issuerURL", body.getString("issuerURL"));
		String response = registration.updateRegistrationDetails(json.toString(), agentId);
		log.info(response);

		int statusCode = registration.getStatusCode();
		if (statusCode != 200) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
			json.put("aimURL", aimUrl);
			utils.saveAgentRegistration(json);
		}
		return json.toString();
	}

	@RequestMapping(path = "/agent/fetch/{agentid}", produces = "application/json")
	public String fetchAgentData(@PathVariable("agentid") String agentId) throws Exception {
		log.info("/agent/fetch/" + agentId);
		JSONObject json = new JSONObject();

		ID4meUtils utils = new ID4meUtils(env);
		String details = utils.getAgentRegistration(agentId);
		if (details == null) {
			log.info("Agent id not found!");
			json.put("error", "Agent id not found!");
			return json.toString();
		}
		json = new JSONObject(details);
		String aimUrl = json.getString("aimURL");
		RegistrationHandler registration = new RegistrationHandler(aimUrl, utils.getPublicKey(), utils.getPrivateKey());
		String response = registration.getRegistrationInfo(agentId);

		log.info(response);
		int statusCode = registration.getStatusCode();
		if (statusCode != 200) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
		}
		return json.toString();
	}

	@RequestMapping(value = "/agent/list", produces = "application/json")
	public String listAgents() throws Exception {
		log.info("/agent/list");
		ID4meUtils utils = new ID4meUtils(env);
		JSONArray array = utils.listAgentRegistrations();
		return array.toString();
	}
}
