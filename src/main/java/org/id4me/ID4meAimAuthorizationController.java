/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

/**
 * org.id4me.demo contains the spring boot application and controller classes, used in the <b>ID4meAimDemoProject</b>.
 */
package org.id4me;

import java.security.Principal;
import java.util.logging.Logger;

import org.id4me.agent.RegistrationHandler;
import org.id4me.utils.ID4meUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring Boot controller with the endpoints related to the identity registration
 * and identitiy management calls of @see <a href=
 * "https://id.denic.de/aim/docs/api/#tag/identity-registration">Identity
 * Registration</a> and @see
 * <a href="https://id.denic.de/aim/docs/api/#tag/identity-management">Identity
 * Management</a>
 * 
 * @author peter.hoebel
 *
 */
@RestController
public class ID4meAimAuthorizationController {
	private static final Logger log = Logger.getGlobal();
	@Autowired
	private Environment env;

	@RequestMapping(value = "/authz/create", consumes = "application/json", produces = "application/json")
	public String createAuthorization(@RequestBody String payload) throws Exception {
		log.info("/authz/create");
		JSONObject json = new JSONObject(payload);
		String agentId = json.getString("agentId");

		String aimUrl = json.getString("aimURL");
		ID4meUtils utils = new ID4meUtils(env);
		RegistrationHandler registration = new RegistrationHandler(aimUrl, utils.getPublicKey(), utils.getPrivateKey());

		JSONObject body = new JSONObject();
		body.put("identifier", json.getString("identifier"));
		String locale = json.has("locale") && !"".equals(json.getString("locale")) ? json.getString("locale") : "DE";
		body.put("locale", locale);
		log.info(body.toString(2));

		String response = registration.createIdentity(body.toString(), agentId);
		log.info(response);
		int statusCode = registration.getStatusCode();
		if (statusCode != 201) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
			json.put("agentid", agentId);
			System.out.println(json.toString(2));
			String sha256 = utils.sha256(json.getJSONObject("challenge").getString("token"));
			String acme = "_acme-challenge." + json.getString("identifier") + ". 300 IN TXT \"" + sha256 + "\"";
			json.put("acme_record", acme);
			utils.saveIdentityAuthorization(json);
		}

		return json.toString();
	}

	@RequestMapping(path = "/authz/list", produces = "application/json")
	public String listAuthorizations() throws Exception {
		log.info("/authz/list");
		ID4meUtils utils = new ID4meUtils(env);
		return utils.listIdentityRegistrations().toString();
	}

	@RequestMapping(path = "/authz/fetch/{identifier}", produces = "application/json")
	public String fetchAuthorizationData(@PathVariable("identifier") String identifier) throws Exception {
		log.info("/authz/fetch/" + identifier);
		JSONObject json = new JSONObject();

		ID4meUtils utils = new ID4meUtils(env);
		String details = utils.getIdentityAuthorization(identifier);
		if (details == null) {
			json.put("error", "Authorization details not found!");
			return json.toString();
		}

		json = new JSONObject(details);
		String agentId = json.getString("agentid");
		Integer id = json.getInt("id");

		String agent = utils.getAgentRegistration(agentId);
		if (agent == null) {
			json.put("error", "Agent details not found!");
			return json.toString();
		}

		json = new JSONObject(agent);
		RegistrationHandler registration = new RegistrationHandler(json.getString("aimURL"), utils.getPublicKey(),
				utils.getPrivateKey());

		String response = registration.getAuthorizationDetails(id, agentId);
		log.info(response);
		int statusCode = registration.getStatusCode();
		if (statusCode != 200) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
			System.out.println(json.toString(2));
			json.put("agentid", agentId);
			String sha256 = utils.sha256(json.getJSONObject("challenge").getString("token"));
			String acme = "_acme-challenge." + json.getString("identifier") + ". 300 IN TXT \"" + sha256 + "\"";
			json.put("acme_record", acme);

			String status = json.getString("status");
			Object validated = json.getJSONObject("challenge").get("validated");
			if ("solved".equals(status) && validated != null) {
				String identity = registration.getIdentityDetails(identifier, agentId);
				if (registration.getStatusCode() == 200) {
					JSONObject ident = new JSONObject(identity);
					System.out.println(ident.toString(2));
					if (!utils.isUrlExpired(ident))
						json.put("magic_url", ident.getString("url"));
				}
			}
			utils.saveIdentityAuthorization(json);
		}
		return json.toString();
	}

	@RequestMapping(value = "/authz/notify/{identifier}", produces = "application/json")
	public String notifyDnsSetupComplete(@PathVariable("identifier") String identifier) throws Exception {
		log.info("/authz/notify/" + identifier);
		JSONObject json = new JSONObject();
		ID4meUtils utils = new ID4meUtils(env);

		String details = utils.getIdentityAuthorization(identifier);
		if (details == null) {
			json.put("error", "Authorization details not found!");
			return json.toString();
		}

		json = new JSONObject(details);
		String agentId = json.getString("agentid");
		Integer id = json.getInt("id");

		String agent = utils.getAgentRegistration(agentId);
		if (agent == null) {
			json.put("error", "Agent details not found!");
			return json.toString();
		}
		json = new JSONObject(agent);

		RegistrationHandler registration = new RegistrationHandler(json.getString("aimURL"), utils.getPublicKey(),
				utils.getPrivateKey());

		String response = registration.notifyDnsComplete(id, agentId);
		log.info("Identity authz: " + response);
		int statusCode = registration.getStatusCode();
		if (statusCode != 200) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
			json.put("agentid", agentId);

			// now it's time to get the identity data
			response = registration.getIdentityDetails(json.getString("identifier"), agentId);
			json = new JSONObject(response);
			log.info("Identity data: " + response);
			statusCode = registration.getStatusCode();
			if (statusCode != 200) {
				String statusMsg = registration.getStatusMessage();
				log.info(statusCode + ", " + statusMsg);
				json.put("error", statusCode + ", " + statusMsg);
			} else {
				json = new JSONObject(response);
				json.put("agentid", agentId);
				utils.saveIdentityDetails(json);
			}
		}

		return json.toString();
	}

	@RequestMapping(path = "/identity/fetch/{identifier}", produces = "application/json")
	public String fetchIdentityDetails(@PathVariable("identifier") String identifier) throws Exception {
		log.info("/identity/fetch/" + identifier);
		JSONObject json = new JSONObject();
		ID4meUtils utils = new ID4meUtils(env);

		String details = utils.getIdentityAuthorization(identifier);
		if (details == null) {
			json.put("error", "Authorization details not found!");
			return json.toString();
		}

		json = new JSONObject(details);
		String agentId = json.getString("agentid");

		String agent = utils.getAgentRegistration(agentId);
		if (agent == null) {
			json.put("error", "Agent details not found!");
			return json.toString();
		}
		json = new JSONObject(agent);

		RegistrationHandler registration = new RegistrationHandler(json.getString("aimURL"), utils.getPublicKey(),
				utils.getPrivateKey());

		String response = registration.getIdentityDetails(identifier, agentId);
		log.info(response);
		int statusCode = registration.getStatusCode();
		if (statusCode != 200) {
			String statusMsg = registration.getStatusMessage();
			log.info(statusCode + ", " + statusMsg);
			json.put("error", statusCode + ", " + statusMsg);
		} else {
			json = new JSONObject(response);
			json.put("agentid", agentId);
			System.out.println(json.toString(2));
		}
		return json.toString();
	}

	@RequestMapping(value = "/userinfos/fetch", produces = "application/json")
	public String fetchUserinfo(Principal principal) throws Exception {
		JSONObject json = new JSONObject();
		if (principal == null) {
			json.put("error", "Principal not found in session!");
			return json.toString();
		}
		String name = principal.getName();
		log.info("/userinfos/get/" + name);

		ID4meUtils utils = new ID4meUtils(env);
		String ident = utils.getIdentityAuthorization(name);
		if (ident == null) {
			json.put("error", "Identity  \"" + name + "\" not found on this server!");
			return json.toString();
		}

		String userinfo = utils.getUserinfos(name);
		json = new JSONObject(userinfo);
		json.put("identifier", name);
		return json.toString();
	}

	@RequestMapping(value = "/userinfos/save", consumes = "application/json", produces = "application/json")
	public String saveUserinfo(Principal principal, @RequestBody String payload) throws Exception {
		JSONObject json = new JSONObject();
		if (principal == null) {
			json.put("error", "Principal not found in session!");
			return json.toString();
		}
		String identifier = principal.getName();
		log.info("/userinfos/save/" + identifier);

		ID4meUtils utils = new ID4meUtils(env);
		String ident = utils.getIdentityAuthorization(identifier);
		if (ident == null) {
			json.put("error", "Identity not found on this server!");
			return json.toString();
		}

		json = new JSONObject(payload);
		JSONObject userinfo = new JSONObject();
		userinfo.put("website", json.getString("website"));
		userinfo.put("zoneinfo", json.getString("zoneinfo"));
		userinfo.put("birthdate", json.getString("birthdate"));
		userinfo.put("address", json.getString("address"));
		userinfo.put("gender", json.getString("gender"));
		userinfo.put("profile", json.getString("profile"));
		userinfo.put("preferred_username", json.getString("preferred_username"));
		userinfo.put("given_name", json.getString("given_name"));
		userinfo.put("middle_name", json.getString("middle_name"));
		userinfo.put("locale", json.getString("locale"));
		userinfo.put("picture", json.getString("picture"));
		userinfo.put("name", json.getString("name"));
		userinfo.put("nickname", json.getString("nickname"));
		userinfo.put("phone_number", json.getString("phone_number"));
		userinfo.put("family_name", json.getString("family_name"));
		userinfo.put("email", json.getString("email"));
		userinfo.put("phone_number_verified", json.getBoolean("phone_number_verified"));
		userinfo.put("email_verified", json.getBoolean("email_verified"));

		utils.saveUserinfos(identifier, userinfo);
		return userinfo.toString();
	}

}
