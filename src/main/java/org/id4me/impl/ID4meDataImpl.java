package org.id4me.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.UUID;

import org.id4me.utils.ID4meData;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;

/**
 * Implementation of ID4meData with local filesystem as storage.
 * 
 * @author peter.hoebel
 *
 */
public class ID4meDataImpl implements ID4meData {
	private Environment env;
	private String dataPath;
	private static final String AGENT_REGISTRATIONS_PATH = "accounts";
	private static final String AUTHZ_REGISTRATIONS_PATH = "authz";
	private static final String IDENTITY_DETAILS_PATH = "identities";
	private static final String USERINFO_DATA_PATH = "userinfo";

	private static final Logger log = LoggerFactory.getLogger(ID4meDataImpl.class);

	/**
	 * Constructor, creates all used directories and sets some local variables.
	 * 
	 * @param env
	 */
	public ID4meDataImpl(Environment env) {
		this.env = env;
		this.dataPath = env.getProperty("id4me.data.path");
		this.dataPath = this.dataPath.endsWith("/") ? this.dataPath : this.dataPath.trim() + "/";
		checkFilePath();
	}

	@Override
	public String getIdentityAuthorization(String identifier) throws Exception {
		return getFileContent(AUTHZ_REGISTRATIONS_PATH + "/" + identifier + ".json", null);
	}

	@Override
	public void saveIdentityAuthorization(JSONObject registration) throws Exception {
		writeFileContent(AUTHZ_REGISTRATIONS_PATH + "/" + registration.getString("identifier") + ".json",
				registration.toString(2));
	}

	@Override
	public void saveIdentityDetails(JSONObject registration) throws Exception {
		writeFileContent(IDENTITY_DETAILS_PATH + "/" + registration.getString("identifier") + ".json",
				registration.toString(2));
	}

	@Override
	public JSONArray listAgentRegistrations() throws Exception {
		JSONArray array = new JSONArray();

		String[] files = getDirectoryContent(AGENT_REGISTRATIONS_PATH);
		if (files != null)
			for (String file : files) {
				String content = getFileContent(AGENT_REGISTRATIONS_PATH + "/" + file, null);
				if (content != null) {
					JSONObject obj = new JSONObject(content);
					array.put(obj);
				}
			}
		return array;
	}

	@Override
	public void saveAgentRegistration(JSONObject registration) throws Exception {
		writeFileContent(AGENT_REGISTRATIONS_PATH + "/" + registration.getString("id") + ".json",
				registration.toString(2));
	}

	@Override
	public String getAgentRegistration(String agentId) throws Exception {
		String content = getFileContent(AGENT_REGISTRATIONS_PATH + "/" + agentId + ".json", null);
		return content;
	}

	/**
	 * Returns the userinfo object for the identity. If no data is found, a default
	 * userinfo object containing all supported claims is returned.
	 * 
	 * @return String representation of the json object
	 */
	@Override
	public String getUserinfos(String name) {
		String userinfo = getFileContent(USERINFO_DATA_PATH + "/" + name + ".json", getDefaultUserinfo());
		return userinfo;
	}

	@Override
	public void saveUserinfos(String identifier, JSONObject userinfo) throws Exception {
		writeFileContent(USERINFO_DATA_PATH + "/" + identifier + ".json", userinfo.toString(2));
	}

	private String[] getDirectoryContent(String name) {
		log.info("getDirectoryContent(), path: " + dataPath + ", " + name);
		File file = new File(dataPath + name);
		if (file.exists() && file.isDirectory()) {
			String[] list = file.list();
			Arrays.sort(list);
			return list;
		}

		return null;
	}

	private String getFileContent(String name, String defaultValue) {
		try {
			log.info("getFileContent(), dataPath, path: " + dataPath + ", " + name);
			File file;
			if (name.startsWith("/")) {
				file = new File(name);
			} else {
				file = new File(dataPath + name);
			}
			FileInputStream in = new FileInputStream(file);
			byte[] buff = new byte[8192];
			int len = in.read(buff);
			in.close();
			return new String(buff, 0, len);
		} catch (Exception ex) {
			return defaultValue;
		}
	}

	private void writeFileContent(String name, String content) throws Exception {
		log.info("writeFileContent(), dataPath, path: " + dataPath + ", " + name);
		File file;
		if (name.startsWith("/")) {
			file = new File(name);
		} else {
			file = new File(dataPath + name);
		}
		FileOutputStream out = new FileOutputStream(file);
		out.write(content.getBytes());
		out.close();
	}

	@Override
	public PrivateKey getPrivateKey() throws Exception {
		String path = env.getProperty("id4me.private.key");
		String key = getFileContent(path, "");
		return getPrivateKeyFromString(key);
	}

	@Override
	public PublicKey getPublicKey() throws Exception {
		String path = env.getProperty("id4me.public.key");
		String key = getFileContent(path, "");
		return getPublicKeyFromString(key);
	}

	private PrivateKey getPrivateKeyFromString(String key) throws Exception {
		String algorithm = "RSA";
		String privKeyPEM = key.replace("-----BEGIN PRIVATE KEY-----\n", "");
		privKeyPEM = privKeyPEM.replace("-----END PRIVATE KEY-----", "");
		byte[] decoded = Base64.getDecoder().decode(privKeyPEM.replaceAll("\\s", ""));

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);
		KeyFactory kf = KeyFactory.getInstance(algorithm);
		return kf.generatePrivate(spec);
	}

	private PublicKey getPublicKeyFromString(String key) throws Exception {
		String algorithm = "RSA";
		String publicKeyPEM = key.replace("-----BEGIN PUBLIC KEY-----\n", "");
		publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");
		byte[] decoded = Base64.getDecoder().decode(publicKeyPEM.replaceAll("\\s", ""));

		X509EncodedKeySpec spec = new X509EncodedKeySpec(decoded);
		KeyFactory kf = KeyFactory.getInstance(algorithm);
		return kf.generatePublic(spec);
	}

	private void checkFilePath() {
		File dir = new File(dataPath + AGENT_REGISTRATIONS_PATH);
		if (!dir.exists())
			dir.mkdirs();
		dir = new File(dataPath + AUTHZ_REGISTRATIONS_PATH);
		if (!dir.exists())
			dir.mkdirs();
		dir = new File(dataPath + IDENTITY_DETAILS_PATH);
		if (!dir.exists())
			dir.mkdirs();
		dir = new File(dataPath + USERINFO_DATA_PATH);
		if (!dir.exists())
			dir.mkdirs();
	}

	/**
	 * The default userinfo object used by {@link #getUserinfos(String)} if an
	 * identity has not already saved its own userinfos. Contains all supported
	 * claims.
	 * 
	 * @return
	 */
	private String getDefaultUserinfo() {
		String userinfo = "{  \"website\": \"\",  \"zoneinfo\": \"\",  \"email_verified\": false,  \"birthdate\": \"\",  \"address\": \"\",  \"gender\": \"\",  \"profile\": \"\",  \"phone_number_verified\": false,  \"preferred_username\": \"\",  \"given_name\": \"\",  \"middle_name\": \"\",  \"locale\": \"\",  \"picture\": \"\",  \"name\": \"\",  \"nickname\": \"\",  \"phone_number\": \"\",  \"family_name\": \"\",  \"email\": \"\"}";
		return userinfo;
	}

	/**
	 * Create a jwks.json file, if not exists.
	 * 
	 * @return
	 */
	
	public synchronized void createJwkIfNotExists() throws Exception {
		String path = env.getProperty("jwks.json");
		String jwks = getFileContent(path, null);
		if (jwks == null) {
			PublicKey pubKey = getPublicKey();
			JWK webkey = new RSAKey.Builder((RSAPublicKey) pubKey).keyUse(KeyUse.SIGNATURE)
					.keyID(UUID.randomUUID().toString()).build();
			JSONArray keys = new JSONArray();
			keys.put(webkey.toJSONObject());
			JSONObject json = new JSONObject();
			json.put("keys",  keys);
			writeFileContent("jwks.json", json.toString(2));
		}

		return;
	}

	@Override
	public String getJwksJson() throws Exception {
		String path = env.getProperty("jwks.json");
		String jwks = getFileContent(path, null);
		return jwks;
	}
}
