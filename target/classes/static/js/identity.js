var accounts = [];
var authz = [];

function init() {
	getAgentList();
	clearAuthorizationDataForm()
}

function createNewAuthorization() {
	var f = document.forms["register_authorization"];
	var json = {};
	json["identifier"] = f.identifier.value;
	json["locale"] = f.locale.value;
	var agent = f["agents"];
	var agentData = JSON.parse(agent[agent.selectedIndex].value);
	json["agentId"] = agentData.id;
	json["aimURL"] = agentData.aimURL;
	var url = "/authz/create";
	postRequest(json, url, onCreateNewAuthorization, function(json) {
		alert(JSON.stringify(json));
	});
}

function onCreateNewAuthorization(json) {
	console.log(JSON.stringify(json));
	showResponseText(json);
	var f = document.forms["show_authorization"];
	onFetchAuthorizationData(json);
}

function fetchAuthorizationData() {
	var f = document.forms["show_authorization"];
	var url = "/authz/fetch/" + f.identifier.value;
	getRequest(url, onFetchAuthorizationData);
}

function onNotifyDnsSetupComplete(json) {
	showResponseText(json);
	//used to open the magic url
	fetchAuthorizationData();
}

function notifyDnsSetupComplete() {
	var f = document.forms["show_authorization"];
	var url = "/authz/notify/" + f.identifier.value;
	getRequest(url, onNotifyDnsSetupComplete);
}

function clearAuthorizationDataForm() {
	var f = document.forms["show_authorization"];
	f.identifier.value = "";
	f.id.value = "";
	f.status.value = "";
	f.agentid.value = "";
	f.expires.value = "";
	f.validated.value = "";
	f.url.value = "";
	f.token.value = "";
	f.acme_record.value = "";

	f.status.style.color = "black";
	f.expires.style.color = "black";

}

function onFetchAuthorizationData(json) {
	clearAuthorizationDataForm();
	showResponseText(json);
	var f = document.forms["show_authorization"];
	f.identifier.value = json.identifier;
	f.id.value = json.id;
	f.status.value = json.status;
	f.agentid.value = json.agentid;
	f.expires.value = json.expires;
	f.validated.value = json.challenge.validated != "" ? json.challenge.validated
			: "";
	f.url.value = json.challenge.url;
	f.token.value = json.challenge.token;
	f.acme_record.value = json.acme_record;

	if ("solved" == json.status) {
		f.status.style.color = "green";
		if (json.magic_url != null) {
			window.open(json.magic_url);
		}
	} else {
		f.status.style.color = "black";
		if (isExpired(json.expires)) {
			f.expires.style.color = "red";
		} else {
			f.expires.style.color = "black";
		}
	}

	f.identifier.focus();
}

function getIdentityAuthzList() {
	var url = "/authz/list";
	getRequest(url, onIdentityAuthzList);
}

function onIdentityAuthzList(json) {
	authz = json;
	console.log(JSON.stringify(accounts, null, "\t"));
	fillIdentityAuthzDropdown();
}

function fillIdentityAuthzDropdown() {
	var f = document.forms["show_authorization"];
	var elem = f.identifier;
	elem.innerHTML = "";
	for (i in authz) {
		var option = document.createElement("option");
		option.value = JSON.stringify(authz[i]);
		option.name = authz[i].identifier;
		option.innerHTML = authz[i].identifier;
		elem.appendChild(option);
	}
	if (authz.length > 0) {
		onIdentityAuthzDropdownChange(JSON.stringify(authz[0]));
	}
}

function onIdentityAuthzDropdownChange(select) {
	var json = JSON.parse(select);
	var f = document.forms["show_authorization"];
	f.id.value = json.id;
	f.status.value = json.status;
	f.agentid.value = json.agentid;
	f.expires.value = json.expires;
	f.validated.value = json.challenge.validated != "" ? json.challenge.validated
			: "";
	f.url.value = json.challenge.url;
	f.token.value = json.challenge.token;

	if (json.status == "solved") {
		f.status.style.color = "green";
	} else {
		f.status.style.color = "black";
	}

	if (isExpired(json.expires)) {
		f.expires.style.color = "red";
	} else {
		f.expires.style.color = "black";
	}
}

// TODO extract to seperate file
// Agent functions are redundant with functions in agent.js
function getAgentList() {
	var url = "/agent/list";
	getRequest(url, onAgentListSuccess);
}

function onAgentListSuccess(json) {
	accounts = json;
	console.log(JSON.stringify(accounts, null, "\t"));
	fillAgentDropdown();
}

function fillAgentDropdown() {
	var f = document.forms["register_authorization"];
	var elem = f.agents;
	elem.innerHTML = "";
	for (i in accounts) {
		var option = document.createElement("option");
		option.value = JSON.stringify(accounts[i]);
		option.name = accounts[i].name;
		option.innerHTML = accounts[i].name;
		elem.appendChild(option);
	}
}
// End of redundant Agent functions
