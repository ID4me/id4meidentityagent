<img src="https://id4me.org/wp-content/uploads/2019/02/logo.png" height="50"/>

ID4me Spring Boot Identity Agent
----------------------------

_Version 1.0.0-SNAPSHOT - 10. October 2019_

This is a sample application, of a Spring Boot identity agent/claims provider

Please note that at this point in time the ID4me service is still experimental. No guarantee is given that the present application, or even the underlying specification and architecture, will not change before final release.


Documentation
-------------

You should first make sure you understand the architecture and basic technical elements of the ID4me platform. 
You can refer to the "Technical Overview" document available in the [Documents section of the ID4me website](https://id4me.org/documents).

## Deployment
To compile an run these application, please **create a directory with write access** (e.g. /opt/id4me/) in your servers file system which can hold the necessary configuration files and store the relying party registrations.
If done, set your properties in application.properties.
 
You can also refer to the **Javadoc code documentation**.

## Authors

* **Peter Höbel** - *Initial work* - [Open Xchange](https://open-xchange.com)

License
--------------------

The library is (C) OX Software GmbH and it is distributed as free software under the MIT license. See the _LICENSE_ file in the distribution.